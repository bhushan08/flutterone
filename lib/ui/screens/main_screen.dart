import 'package:flutter/material.dart';
import 'package:onboarding_flow/business/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:onboarding_flow/models/user.dart';
import 'TabbarScreens/activityScreen/activityScreen.dart';
import 'TabbarScreens/dashboardScreen/dashBoardScreen.dart';
import 'TabbarScreens/notificationScreen/notificationScreen.dart';
import 'TabbarScreens/orderScreen/orderScreen.dart';
import 'TabbarScreens/homeScreen/homeScreen.dart';
import 'package:onboarding_flow/ui/configuration/configFile.dart';
import 'package:flutter/cupertino.dart';

class DrawerItem {
  String title;
  IconData icon;
  DrawerItem(this.title, this.icon);
}

class MainScreen extends StatefulWidget {
  final FirebaseUser firebaseUser;

  MainScreen({this.firebaseUser});

  _MainScreenState createState() => _MainScreenState();

  final drawerItemList = [
    new DrawerItem("Home", Icons.home),
    new DrawerItem("Dashboard", Icons.dashboard),
    new DrawerItem("Orders", Icons.library_books),
    new DrawerItem("Notifications", Icons.notifications),
    new DrawerItem("Home", Icons.assignment),
  ];
}

class _MainScreenState extends State<MainScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    print(widget.firebaseUser);
  }

  int _selectedDrawerIndex = 0;

  _getDrawerItemWidget(int pos){
    switch (pos) {
      case 0: return new homeScreenClass();
      case 1: return new dashboardScreenClass();
      case 2: return new orderScreenClass(false);
      case 3: return new notificationScreenClass();
      case 4: return new activityScreenClass();
        
        break;
      default: return new Text("Error");
    }
  }

  _onSelectItems(int index){
    setState(() => _selectedDrawerIndex = index);
    Navigator.of(context).pop();
  }

  Widget callPage(int currentTabIndex){
    switch (currentTabIndex) {

      case 0: return homeScreenClass();
      case 1: return dashboardScreenClass();
      case 2: return orderScreenClass(false);
      case 3: return notificationScreenClass();
      case 4: return activityScreenClass();
        
        break;
      default:
    }
  }

  String titleName;

  String callTitle(int currentTabIndex){

     switch (currentTabIndex) {

      case 0: return titleName = "Home";
      case 1: return titleName = "Dashboard";
      case 2: return titleName = "Orders";
      case 3: return titleName = "Notifications";
      case 4: return titleName = "Activity";
        
        break;
      default:
    }
  }

  int currentTabIndex = 0;
  onTapped(int index){
    setState(() {
     currentTabIndex = index; 
    });
  }

  @override
  Widget build(BuildContext context) {

    void moveToLastScreen(){
      Navigator.of(context).pop();
    }

    Future<bool> _onBackPressed(){
    return showDialog(
      context: context,
      builder: (context)=> AlertDialog(
        title: Text("Do You Want to Exit Application?"),
        actions: <Widget>[
          FlatButton(
            child: Text("No"),
            onPressed: ()=>Navigator.of(context).pop(false),
          ),
          FlatButton(
            child: Text("Yes"),
            onPressed: ()=>Navigator.of(context).pop(true),
          )
        ],
      )
    );
  }

    var drawerOptions = <Widget>[];
    for (var i = 0; i < (widget.drawerItemList.length); i++) {

        var d = widget.drawerItemList[i];
        drawerOptions.add(
          new ListTile(
            leading: new Icon(d.icon),
            title: new Text(d.title),
            selected: i ==_selectedDrawerIndex,
            onTap: () => _onSelectItems(i),
          )
        );
      }

    //configFile.dart is the configuration file to change Drawer to
    //Bottom Navigation Bar 
    //if the condition of if loop is true Drawer will be reflected
    //and if condition is turns out to be false (which can be changed by
    //going to configFile.dart and changing the return from true to false
    //of acivateDrawerClass) then Bottom Navigation Bar will be activaed
    //for the application

    if (configFileClass.activateDrawerMethod()){
      return WillPopScope(
        onWillPop: _onBackPressed,
              child: Scaffold(

          /*
          //Was trying to create an Expandable appbar but then realised iphone X notch will get affected
          drawer: Drawer(),
          body: NestedScrollView(
            headerSliverBuilder: (BuildContext contex, bool innerBoxIsScrolled){
                return <Widget>[
                  SliverAppBar(
                    elevation: 0.0,
                    expandedHeight: 200.0,
                    floating: false,
                    pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                      centerTitle: true,
                      title: Text(callTitle(currentTabIndex)),
                    ),
                    )
                ];
              },
              body: callPage(currentTabIndex),
            ),
          );
          */

          key: _scaffoldKey,
          appBar: new AppBar(
            title: new Text(widget.drawerItemList[_selectedDrawerIndex].title),
            centerTitle: true,
            backgroundColor: Colors.white,
            elevation: 0.0,
          ),
          drawer: Drawer(
            child: Column(
              children: <Widget>[
                UserAccountsDrawerHeader(
                  accountName: new Text("User"),
                  accountEmail: null,
                ),
                Column(
                  children: drawerOptions,
                )
              ],
            ),
          ),
          body: _getDrawerItemWidget(_selectedDrawerIndex),
        ),
      );
      } else {
      return CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
          items: <BottomNavigationBarItem> [
            // ...
            BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.home),
              title: Text('Home')
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.dashboard),
                title: Text('Dashboard')
            ),
            BottomNavigationBarItem(
                icon: Icon(CupertinoIcons.add_circled),
                title: Text('Orders')
            ),
            BottomNavigationBarItem(
                icon: Icon(CupertinoIcons.add_circled),
                title: Text('Notifications')
            ),
            BottomNavigationBarItem(
                icon: Icon(CupertinoIcons.add_circled),
                title: Text('Activity')
            )
          ],
        ),
        tabBuilder: (BuildContext context, int index) {
          return CupertinoTabView(
            builder: (BuildContext context) {
              return Scaffold(
                appBar: CupertinoNavigationBar(
                  middle: Text(callTitle(index)),
                ),
                body: callPage(index)
              );
            },
          );
        },
      );
    } 
      
      
      /*StreamBuilder(
        stream: Auth.getUser(widget.firebaseUser.uid),
        builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Color.fromRGBO(212, 20, 15, 1.0),
                ),
              ),
            );
          } else {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 100.0,
                    width: 100.0,
                    child: CircleAvatar(
                      backgroundImage: (snapshot.data.profilePictureURL != '')
                          ? NetworkImage(snapshot.data.profilePictureURL)
                          : AssetImage("assets/images/default.png"),
                    ),
                  ),
                  Text("Name: ${snapshot.data.firstName}"),
                  Text("Email: ${snapshot.data.email}"),
                  Text("UID: ${snapshot.data.userID}"),
                ],
              ),
            );
          }
        },
      ),*/
  }

  void _logOut() async {
    Auth.signOut();
  }
}
