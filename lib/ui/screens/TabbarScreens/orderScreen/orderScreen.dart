import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/orderDetailScreen/orderDetailScreen.dart';
import 'dart:convert';
import '../../../configuration/configFile.dart';
import 'package:http/http.dart' as http;

class orderScreenClass extends StatefulWidget {
  @override
  _OrderFetchDataState createState() => _OrderFetchDataState(isCommingFromHorizontalList);

  final bool isCommingFromHorizontalList;
  orderScreenClass(this.isCommingFromHorizontalList);
}

class _OrderFetchDataState extends State<orderScreenClass> {
  List<OrdersDataClass> list = List();
  var isLoading = false;
  bool isCommingFromHorizontalList;
  _OrderFetchDataState(this.isCommingFromHorizontalList);
  _fetchData() async {
    setState(() {
      isLoading = true;
    });
    final response =
        await http.get("https://jsonplaceholder.typicode.com/users");
    if (response.statusCode == 200) {
      list = (json.decode(response.body) as List)
          .map((data) => new OrdersDataClass.fromJson(data))
          .toList();
      setState(() {
        isLoading = false;
      });
    } else {
      throw Exception('Failed to load photos');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
        appBar: ( isCommingFromHorizontalList)?
            CupertinoNavigationBar(
              middle: Text("Orders"),
            ):null,
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: list.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    child: Padding(
                      padding:
                          EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
                      child: Container(
                        height: 100.0,
                        color: Colors.white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: 70,
                                    width: 70,
                                    child: Icon(CupertinoIcons.profile_circled,size: 75,),
                                    /*decoration: BoxDecoration(
                                            image: DecorationImage(
                                              //image: AssetImage(imgPath),
                                              fit: BoxFit.contain
                                            )
                                        ),*/
                                  ),
                                  SizedBox(
                                    width: 20.0,
                                  ),
                                  Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              //Product Name container
                                              height: 20.0,
                                              child: Text(
                                                  list[index].productName,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                            )
                                          ],
                                        ),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              //Customer Name Container
                                              height: 20.0,
                                              child: Text(
                                                list[index].productName,
                                                style:
                                                    TextStyle(fontSize: 12.0),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 15.0,
                                            ),
                                            Container(
                                              //PaymentModeContainer
                                              height: 20.0,
                                              child: Text(
                                                "Android",
                                                style:
                                                    TextStyle(fontSize: 12.0),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 15.0,
                                            ),
                                            Container(
                                              //OrderId Container
                                              height: 20.0,
                                              child: Text(
                                                "323474",
                                                style:
                                                    TextStyle(fontSize: 12.0),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 15.0,
                                            ),
                                            Container(
                                              //Date of Purchase Container
                                              height: 20.0,
                                              child: Text(
                                                "Aug 12",
                                                style:
                                                    TextStyle(fontSize: 12.0),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ]),
                                ],
                              ),
                            ),
                            Container(
                              //Const of Profuct Container
                              child: Text(
                                "\$123",
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blue),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).push(CupertinoPageRoute<void>(
                          builder: (BuildContext context) => orderDetailScreen(
                                ordersDataClass: list[index],
                              )));
                    },
                  );
                }));
  }

  @override
  void initState() {
    super.initState();
    this._fetchData();
  }
}

/*
var orderDataArray = [

    OrdersDataClass("Daniel Wellington Classic", "John DOe", "Strip", "#51238249", "Aug 11",123),
    OrdersDataClass("Skater Dress", "Adele Camp", "Square", "#51238249", "Aug 11",260),
    OrdersDataClass("The Mask", "Harvey Dent", "Paypal", "#51238249", "Aug 11",500),
    OrdersDataClass("Daniel Wellington Classic", "John DOe", "Strip", "#51238249", "Aug 11",123),
    OrdersDataClass("Skater Dress", "Adele Camp", "Square", "#51238249", "Aug 11",260),
    OrdersDataClass("The Mask", "Harvey Dent", "Paypal", "#51238249", "Aug 11",500),    
    OrdersDataClass("Daniel Wellington Classic", "John DOe", "Strip", "#51238249", "Aug 11",123),
    OrdersDataClass("Skater Dress", "Adele Camp", "Square", "#51238249", "Aug 11",260),
    OrdersDataClass("The Mask", "Harvey Dent", "Paypal", "#51238249", "Aug 11",500),
];
*/

class OrdersDataClass {
  //final String imgPath;
  final String productName;
  final String customerName;
  //final String paymentMode;
  //final String orderId;
  //final String dateOfPurchase;
  //final double costOfProduct;

  OrdersDataClass._({
    /*this.imgPath,*/ this.productName,
    this.customerName,
    /*this.paymentMode, this.orderId, this.dateOfPurchase, this.costOfProduct*/
  });

  factory OrdersDataClass.fromJson(Map<String, dynamic> json) {
    return new OrdersDataClass._(
      productName: json['name'],
      customerName: json['name'],
      //paymentMode:json['username'],
      //orderId:json[''],
      //dateOfPurchase,
      //costOfProduct
    );
  }
}
