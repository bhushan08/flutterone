import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:flutter/cupertino.dart';

class analyticsScreenClass extends StatelessWidget{

  @override
  Widget build(BuildContext context) {


    var lineData = [
      LinearSales(0, 43),
      LinearSales(1, 20),
      LinearSales(2, 30),
      LinearSales(3, 15),
      LinearSales(4, 22),
      LinearSales(5, 52),
      LinearSales(6, 32),
      LinearSales(7, 62),
      LinearSales(8, 72),
      LinearSales(9, 92),
      LinearSales(10, 82),
    ];

    var lineSeries = [
      charts.Series(domainFn: (LinearSales sales,_) => sales.year,
      measureFn: (LinearSales sales,_) => sales.linear_sales,
      fillColorFn: (_,__) => charts.MaterialPalette.blue.shadeDefault,
      id:  "Sales",
      data: lineData
      )
    ];

    var lineChart = charts.LineChart(
      lineSeries,
      animate: true,
      defaultRenderer: new charts.LineRendererConfig(includePoints: true, includeArea: true),
      primaryMeasureAxis: new charts.NumericAxisSpec(
        renderSpec: new charts.SmallTickRendererSpec()
      ),
    );


    var revenueData = [
      Sales("Sun", 50),
      Sales("Mon", 70),
      Sales("Tue", 100),
      Sales("Wed", 50),
      Sales("Thurs", 150),
      Sales("Fri", 190),
      Sales("Sat", 132),
    ];

    var series = [
      charts.Series(domainFn: (Sales  sales,_) => sales.day,
        measureFn: (Sales sales,_) => sales.sold,
        id: "Sales",
        data: revenueData
      ),
    ];

    var revenuChart = charts.BarChart(
      series,
      animate: true,
      behaviors: [new charts.SeriesLegend()],
      defaultInteractions: true,
      primaryMeasureAxis: new charts.NumericAxisSpec(
        renderSpec: new charts.SmallTickRendererSpec()
      )
    );

    var revenueCostsData = [
      Sales("Sun", 50),
      Sales("Mon", 120),
      Sales("Tue", 100),
      Sales("Wed", 50),
      Sales("Thurs", 250),
      Sales("Fri", 190),
      Sales("Sat", 132),
    ];

    var series1 = [
      charts.Series(domainFn: (Sales  sales,_) => sales.day,
        measureFn: (Sales sales,_) => sales.sold,
        id: "Sales",
        data: revenueCostsData
      ),
    ];

    var revenuChartByCountry = charts.BarChart(
      series1,
      animate: true,
      behaviors: [new charts.SeriesLegend()],
      defaultInteractions: true,

      primaryMeasureAxis: new charts.NumericAxisSpec(
        renderSpec: new charts.SmallTickRendererSpec()
      )

    );

    var costChart = charts.BarChart(
      series1,
      animate: true,
      behaviors: [new charts.SeriesLegend()],
      defaultInteractions: true,
    );

    var pie =charts.PieChart(
      series,
      animate: true,
      defaultRenderer: new charts.ArcRendererConfig(
        arcRendererDecorators: [new charts.ArcLabelDecorator()]
      ),
    );

    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Scaffold(

        appBar: CupertinoNavigationBar(
          middle: Text("Analytics"),
        ),

        body: ListView(
          children: <Widget>[
            Container(
              height: 200.0,
              child: revenuChart,
            ),
            Container
            (
              height: 250,
              child: lineChart,
            ),
            Container(
              height: 200.0,
              child: costChart,
            ),
            Container(
              height: 200.0,
              child: pie,
            ),
            Container(
              height: 300.0,
              child: ListView(
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.all(10.0),
                    height: 50.0,
                    child: Text(
                      "Quaterly Revenue by Country",
                      style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
                      ),
                  ),
                  Container(
                    height: 250,
                    child: revenuChartByCountry,
                  ),
                ],

              ),
            ),
          ],
        ),

        ),
    );
  }
}

class LinearSales {
  final int year;
  final int linear_sales;

  LinearSales(this.year, this.linear_sales);
}

class Sales {
  final String day;
  final int sold;

  Sales(this.day, this.sold); 
}