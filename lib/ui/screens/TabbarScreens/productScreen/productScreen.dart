import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/productDetailScreen/productDetailScreen.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;

class ProductScreen extends StatefulWidget {
  @override
  _ProductFetchDataState createState() => _ProductFetchDataState();
}

class _ProductFetchDataState extends State<ProductScreen> {
  List<ProductDataClass> list = List();
  var isLoading = false;

  _fetchData() async {
    setState(() {
      isLoading = true;
    });
    final response =
        await http.get("https://jsonplaceholder.typicode.com/users");
    if (response.statusCode == 200) {
      list = (json.decode(response.body) as List)
          .map((data) => new ProductDataClass.fromJson(data))
          .toList();
      setState(() {
        isLoading = false;
      });
    } else {
      throw Exception('Failed to load photos');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CupertinoNavigationBar(
          middle: Text("Products"),

        ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: list.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    child: Padding(
                      padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
                      child: GestureDetector(
                        child: Container(
                          height: 100.0,
                          color: Colors.white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        height: 70,
                                        width: 70,
                                        /*decoration: BoxDecoration(
                                            image: DecorationImage(
                                              //image: AssetImage(imgPath),
                                              fit: BoxFit.contain
                                            )
                                        ),*/
                                      ),
                                      SizedBox(
                                        width: 20.0,
                                      ),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              height: 20.0,
                                              child: Text(
                                                list[index].productName,
                                                style:TextStyle(
                                                  fontWeight:FontWeight.bold,
                                                )
                                              ),
                                            )
                                          ],
                                        ),
                                        Row(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              height: 20.0,
                                              child: Text(
                                                list[index].productDescription,
                                                style:TextStyle(
                                                  fontSize: 12.0
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ]
                                      ),
                                    ],
                                  ),
                              ),
                              Container(
            //Date of Products container
                                child: Text(
                                  "15 july",
                                style: TextStyle(
                                  fontSize: 12.0,
                                ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    onTap: (){
                      Navigator.of(context).push(CupertinoPageRoute<void>(builder: (BuildContext context) => productDetailScreenClass(productDataClass: list[index],)));
                    },
                  );
                }
              )
            );
  }
  @override
  void initState(){
    super.initState();
    this._fetchData();
  }
}



class ProductDataClass{
  final String productName;
  final String productDescription;
  //final String dateOfProduct;

  ProductDataClass._({this.productName, this.productDescription, /*this.dateOfProduct*/});

  factory ProductDataClass.fromJson(Map<String, dynamic> json){
    return new ProductDataClass._(
      productName: json['name'],
      productDescription: json['name'],
      //dateOfProduct: json['']
    );
  }
}