import 'package:flutter/material.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/productScreen/productScreen.dart';
import 'package:flutter/cupertino.dart';

class productDetailScreenClass extends StatelessWidget{

  final ProductDataClass productDataClass;

  productDetailScreenClass({Key key, @required this.productDataClass}) : super(key : key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: CupertinoNavigationBar(
        middle: Text(productDataClass.productName),
      ),
      body: ListView.builder(
        itemCount: 1,
        itemBuilder: (BuildContext context, int index) => productDetailTile(),
      ),
    );
  }

  Widget productDetailTile(){

    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        child: Column(
          children: <Widget>[
            Container(
              height: 200.0,
              color: Colors.teal,
              child: Text(
                "Image",
                style: TextStyle(
                  fontSize: 40.0
                ),
              ),
            ),
            Row(
              children: <Widget>[
                Container(
                  child: Text(
                    productDataClass.productName,
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                )
              ],
            ),
            Row(
              children: <Widget>[
                Flexible(
                  child: Container(
                    child: Text("sfuidgbalsgfkja sydf aytwfd ytawfdyta wftdjfawye fdyjwf eaytwfv jeyf awjfd jtwea ftydafw ytdf jywaf djyfwed jawfd ytfwe ytdef daytjfwed ytwe fkuwalfi yawle98yf ywaefgwey a9wu e9fy a7w f" 
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Name",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    productDataClass.productName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Item Sold",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    productDataClass.productName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Tags",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    productDataClass.productName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Categories",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    productDataClass.productName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Price",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    productDataClass.productName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "SKU",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    productDataClass.productName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Tax Status",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    productDataClass.productName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Tax Class",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    productDataClass.productName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
          ],
        )
      ),
    );
  }
}