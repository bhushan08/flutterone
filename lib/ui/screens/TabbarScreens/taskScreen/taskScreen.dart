import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';

class taskScreenClass extends StatefulWidget {
  @override
  _TaskFetchDataState createState() => _TaskFetchDataState();
}

class _TaskFetchDataState extends State<taskScreenClass> {
  List<TaskDataClass> list = List();
  var isLoading = false;

  _fetchData() async {
    setState(() {
      isLoading = true;
    });
    final response =
        await http.get("https://jsonplaceholder.typicode.com/photos");
    if (response.statusCode == 200) {
      list = (json.decode(response.body) as List)
          .map((data) => new TaskDataClass.fromJson(data))
          .toList();
      setState(() {
        isLoading = false;
      });
    } else {
      throw Exception('Failed to load photos');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CupertinoNavigationBar(
          middle: Text("Tasks"),
        ),
        backgroundColor: Colors.white,
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: list.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    child: Padding(
                      padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
                        child: Container(
                          height: 100.0,
                          color: Colors.white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                 child: Container(
                                    child: Row(
                                      children: <Widget>[
                                        SizedBox(
                                          width: 20.0,
                                        ),
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                          Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                                Flexible(
                                                  flex: 2,
                                                  //Task Name
                                                  child: Text(
                                                    list[index].taskName,
                                                    style:TextStyle(
                                                      fontWeight:FontWeight.bold,
                                                    )
                                                  ),
                                                ),
                                            ],
                                          ),
                                          Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                //Due Date Container
                                                height: 20.0,
                                                child: Text(
                                                  "Dute Date",
                                                  style:TextStyle(
                                                    fontSize: 12.0
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ]
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                //Progress Container
                                child: Text(
                                  "Done",
                                style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue
                                ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                  );
                }
              )
            );
  }

  @override
  void initState(){
    super.initState();
    this._fetchData();
  }
}

class TaskDataClass {
  
  final String taskName;
  //final String dueDate;
  //final String taskProgress;

  TaskDataClass._({this.taskName,/* this.dueDate, this.taskProgress*/});

  factory TaskDataClass.fromJson(Map<String, dynamic> json){

    return new TaskDataClass._(
      taskName: json['title'],
      //dueDate: json[''],
      //taskProgress: json['']
    );

  }
}
