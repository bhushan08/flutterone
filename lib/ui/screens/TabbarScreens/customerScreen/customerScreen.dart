import 'package:onboarding_flow/ui/screens/TabbarScreens/customerDetailScreen/customerDetailScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';

class customerScreenClass extends StatefulWidget {
  @override
  _CustomerFetchDataState createState() => _CustomerFetchDataState();
}

class _CustomerFetchDataState extends State<customerScreenClass> {
  List<CustomerDataClass> list = List();
  var isLoading = false;

  _fetchData() async {
    setState(() {
      isLoading = true;
    });
    final response =
        await http.get("https://jsonplaceholder.typicode.com/users");
    if (response.statusCode == 200) {
      list = (json.decode(response.body) as List)
          .map((data) => new CustomerDataClass.fromJson(data))
          .toList();
      setState(() {
        isLoading = false;
      });
    } else {
      throw Exception('Failed to load photos');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CupertinoNavigationBar(
          middle: Text("Customers"),
        ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: list.length,
                itemBuilder: (BuildContext context, int index) {
                  var text = Text(
                                              "Platform",
                                              style:
                                                  TextStyle(fontSize: 12.0,color: Colors.grey),
                                            );
                  return GestureDetector(
                    child: Container(
                      padding: EdgeInsets.only(top: 15,left: 15,right: 15),
                      height: 100.0,
                      color: Colors.white,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Row(
                              children: <Widget>[
                                Container(
                                  //Image Container
                                  child:Icon(CupertinoIcons.profile_circled,size: 75,)
                                ),
                                SizedBox(
                                  width: 20.0,
                                ),
                                Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            //Name Container
                                            height: 20.0,
                                            child: Text(
                                                list[index]
                                                    .customerDataClassCustomerName,
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                )),
                                          )
                                        ],
                                      ),
                                      Row(
                                        //Platform Container
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            height: 20.0,
                                            child: text,
                                          ),
                                          SizedBox(
                                            width: 15.0,
                                          ),
                                          Container(
                                            //MoneySpent COntainer
                                            height: 20.0,
                                            child: Text(
                                              "\$",
                                              style:
                                                  TextStyle(fontSize: 12.0),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ]),
                              ],
                            ),
                          ),
                          Container(
                            //Date Conatainer
                            child: Text(
                              "Created on {date}",
                              style: TextStyle(
                                fontSize: 12.0,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).push(CupertinoPageRoute<void>(
                          builder: (BuildContext context) =>
                              customerDetailScreen(
                                customerDataClass: list[index],
                              )));
                    },
                  );
                }));
  }

  @override
  void initState() {
    super.initState();
    this._fetchData();
  }
}

class CustomerDataClass {
  //final String customerDataClassCustomerImage;
  final String customerDataClassCustomerName;
  //final String customerDataClassPlatform;
  //final int customerDataClassSpentMoney;
  //final String customerDataClassdDate;

  CustomerDataClass._(
      {/*this.customerDataClassCustomerImage,*/ this.customerDataClassCustomerName /*, this.customerDataClassPlatform, this.customerDataClassSpentMoney, this.customerDataClassdDate*/});

  factory CustomerDataClass.fromJson(Map<String, dynamic> json) {
    return new CustomerDataClass._(
      customerDataClassCustomerName: json['name'],
      //customerDataClassPlatform : json['id'],
      //customerDataClassSpentMoney : json['id'],
      //customerDataClassdDate : json['suite']
    );
  }
}
