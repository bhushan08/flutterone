import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/orderScreen/orderScreen.dart';
import 'package:flutter/cupertino.dart';

class orderDetailScreen extends StatelessWidget {

  final OrdersDataClass ordersDataClass;

  orderDetailScreen({Key key, @required this.ordersDataClass}): super(key:key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: CupertinoNavigationBar(
        middle: Text(ordersDataClass.productName),
      ),
      body: ListView.builder(
        itemCount: 1,
        itemBuilder: (BuildContext context, int index) => orderDetailTile(),
      ),
    );
  }

  Widget orderDetailTile(){

    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        child: Column(
          children: <Widget>[
            Container(
              height: 200.0,
              color: Colors.teal,
              child: Text(
                "Image",
                style: TextStyle(
                  fontSize: 40.0
                ),
              ),
            ),
            Row(
              children: <Widget>[
                Container(
                  child: Text(
                    ordersDataClass.productName,//Passed data from last Screen
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                )
              ],
            ),
            Row(
              children: <Widget>[
                Flexible(
                  child: Container(
                    child: Text("sfuidgbalsgfkja sydf aytwfd ytawfdyta wftdjfawye fdyjwf eaytwfv jeyf awjfd jtwea ftydafw ytdf jywaf djyfwed jawfd ytfwe ytdef daytjfwed ytwe fkuwalfi yawle98yf ywaefgwey a9wu e9fy a7w f" 
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Creation Date",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    ordersDataClass.productName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Product Name",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    ordersDataClass.productName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Customer Name",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    ordersDataClass.productName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Payment Gateway",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    ordersDataClass.productName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Refund",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    ordersDataClass.productName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Receipt Number",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    ordersDataClass.productName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Final Price",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    ordersDataClass.productName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
          ],
        )
      ),
    );
  }
  
}