import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/homeScreen/homeScreen.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/taskScreen/taskScreen.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/productScreen/productScreen.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/salesScreen/salesScreen.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/orderScreen/orderScreen.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/analyticsScreen/analyticsScreen.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/customerScreen/customerScreen.dart';

class dashboardScreenClass extends StatelessWidget{

  //final String title;
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 100),
        child: GridView.count(
          crossAxisCount: 3,
          padding: const EdgeInsets.all(10.0),
          crossAxisSpacing: 10.0,
          mainAxisSpacing: 10.0,
          children: <Widget>[
            GestureDetector(
              child: Container(
                child: Text("Analytics"),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0)
                  )
                ),
              ),
              onTap: (){
                Navigator.of(context).push(CupertinoPageRoute<void>(builder: (BuildContext context) => analyticsScreenClass()));
                print("tapped on ANALYTICS");
              },
            ),
            GestureDetector(
              child: Container(
                child: (Text("Customers")),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0)
                  )
                ),
              ),
              onTap: (){
                Navigator.of(context).push(CupertinoPageRoute<void>(builder: (BuildContext context) => customerScreenClass()));
                print("tapped on CUSTOMER");
              },
            ),
            GestureDetector(
              child: Container(
                child: Text("Orders"),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0)
                  )
                ),
              ),
              onTap: (){
                Navigator.of(context).push(CupertinoPageRoute<void>(builder: (BuildContext context) => orderScreenClass(true)));
                print("tapped on ORDERS");
              },
            ),
            GestureDetector(
              child: Container(
                child: Text("Tasks"),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0)
                  )
                ),
              ),
              onTap: (){
                Navigator.of(context).push(CupertinoPageRoute(builder:(BuildContext context) => taskScreenClass()));
              },
            ),
            GestureDetector(
              child: Container(
                child: Text("Sales"),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0)
                  )
                ),
              ),
              onTap: (){
                Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>salesScreenClass()));
              },
            ),
            GestureDetector(
              child: Container(
                child: Text("Products"),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0)
                  )
                ),
              ),
              onTap: (){
                Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>ProductScreen()));
              },
            ),
            ],
        ),
      ),
    );
  }
}

