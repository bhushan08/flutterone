import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class notificationScreenClass extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: ListView.builder(
        itemBuilder: (context,index){
          return Row(children: <Widget>[
            Icon(CupertinoIcons.profile_circled,size: 75,),
            Expanded(child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(index.toString()),
                Text("Notification #${index}"),
                Text("Few Days ago  ")
              ],
            ),)
          ],);
        },
      ),
    );
  }
}