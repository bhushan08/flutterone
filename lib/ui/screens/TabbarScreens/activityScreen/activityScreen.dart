import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';

class activityScreenClass extends StatefulWidget {
  @override
  _ActivityFetchDataState createState() => _ActivityFetchDataState();
}

class _ActivityFetchDataState extends State<activityScreenClass> {
  List<ActivityDataClass> list = List();
  var isLoading = false;

  _fetchData() async {
    setState(() {
      isLoading = true;
    });
    final response =
        await http.get("https://jsonplaceholder.typicode.com/users");
    if (response.statusCode == 200) {
      list = (json.decode(response.body) as List)
          .map((data) => new ActivityDataClass.fromJson(data))
          .toList();
      setState(() {
        isLoading = false;
      });
    } else {
      throw Exception('Failed to load photos');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: list.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
                      child: Container(
                        height: 100.0,
                        color: Colors.white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      height: 70,
                                      width: 70,
                                      /*decoration: BoxDecoration(
                                          image: DecorationImage(
                                            //image: AssetImage(imgPath),
                                            fit: BoxFit.contain
                                          )
                                      ),*/
                                    ),
                                    SizedBox(
                                      width: 20.0,
                                    ),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                      Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                            Container(
                                              //Task Name
                                              child: Text(
                                                list[index].activityName,
                                                style:TextStyle(
                                                  fontWeight:FontWeight.bold,
                                                )
                                              ),
                                            ),
                                        ],
                                      ),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            //Due Date Container
                                            height: 20.0,
                                            child: Text(
                                              "Dute Date",
                                              style:TextStyle(
                                                fontSize: 12.0
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ]
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              //Progress Container
                              child: Text(
                                "Done",
                              style: TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.blue
                              ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                }
              )
            );
  }

  @override
  void initState(){
    super.initState();
    this._fetchData();
  }
}

class ActivityDataClass {
  
  final String activityName;
  //final String dueDate;
  //final String activityProgress;

  ActivityDataClass._({this.activityName,/* this.dueDate, this.activityProgress*/});

  factory ActivityDataClass.fromJson(Map<String, dynamic> json){

    return new ActivityDataClass._(
      activityName: json['name'],
      //dueDate: json[''],
      //activityProgress: json['']
    );

  }
}
