import 'package:flutter/material.dart';
import 'package:onboarding_flow/ui/widgets/horizontal_list.dart';
import 'package:onboarding_flow/ui/widgets/overview_tile.dart';
import 'package:onboarding_flow/ui/widgets/recent_orders.dart';
import 'package:onboarding_flow/ui/widgets/total_summary.dart';
import 'package:flutter/cupertino.dart';

class homeScreenClass extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      /*appBar: AppBar(
        title: Text("Home"),
        centerTitle: true,
        elevation: 0.0,
      ),*/
      /*drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text("Bhushan"),
              accountEmail: new Text("wew@gmail.com"),
              currentAccountPicture: new CircleAvatar(
                backgroundColor: Colors.white,
                child: new Text("B", style: TextStyle(color: const Color(0xFF000000)),),
              ),
            ),
            new ListTile(
              title: new Text("Page One"),
              trailing: new Icon(Icons.arrow_upward),
            ),
            new ListTile(
              title: new Text("Page Two"),
              trailing: new Icon(Icons.arrow_forward),
            ),
            new ListTile(
              title: new Text("Page Three"),
              trailing: new Icon(Icons.arrow_upward),
            ),
          ],
        ),
      ),*/
      body: new ListView(
        children: <Widget>[
          new Padding(
            padding: const EdgeInsets.all(8.0),
          ),
          horizontalList(),
          overViewTile(),
          totalSummaryHomeScreen(),
          recentOrders(),
        ],
      ),
    );
  }
}