import 'package:onboarding_flow/ui/screens/TabbarScreens/customerScreen/customerScreen.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';

class customerDetailScreen extends StatelessWidget {

  final CustomerDataClass customerDataClass;

  customerDetailScreen({Key key, @required this.customerDataClass}) : super(key : key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: CupertinoNavigationBar(
        middle: Text(customerDataClass.customerDataClassCustomerName),
      ),
      body: ListView.builder(
        itemCount: 1,
        itemBuilder: (BuildContext context, int index) => customerDetailTile(),
      ),
    );
  }

  Widget customerDetailTile(){

    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        child: Column(
          children: <Widget>[
            Container(
              height: 200.0,
              color: Colors.teal,
              child: Image.network('https://picsum.photos/250?image=9')
            ),
            Row(
              children: <Widget>[
                Container(
//Customer Name Container
                  child: Text(
                    customerDataClass.customerDataClassCustomerName,//Passed data from last Screen
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                )
              ],
            ),
            Row(
              children: <Widget>[
                Flexible(
                  child: Container(
//Customer platform container
                    child: Text(
                      "customerDataClass.customerDataClassPlatform"
                    ),
                  ),
                ),
                SizedBox(
                  width: 15.0,
                ),
                Flexible(
                  child: Container(
//Spent Money Container
                    child: Text(
                      "\${customerDataClass.customerDataClassSpentMoney.toString()}"
                    ),
                  ),
                ),
              ],
            ),
            Divider(),
            Row(
//Name Row
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Name",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    customerDataClass.customerDataClassCustomerName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
//Email Row
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Email",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    customerDataClass.customerDataClassCustomerName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
//Account Created Row
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Account Created",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    customerDataClass.customerDataClassCustomerName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
//Total Spent ROw
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Total Spend",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    customerDataClass.customerDataClassCustomerName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Row(
//Total Orders Row
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    "Total Orders",
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    customerDataClass.customerDataClassCustomerName,
                    style: TextStyle(
                      fontSize: 17.0
                    ),
                  ),
                )
              ],
            ),
            Divider(),
          ],
        )
      ),
    );
  }
}