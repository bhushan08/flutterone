import 'package:flutter/material.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/homeScreen/homeScreen.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/orderScreen/orderScreen.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/analyticsScreen/analyticsScreen.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/customerScreen/customerScreen.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/productScreen/productScreen.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/salesScreen/salesScreen.dart';
import 'package:onboarding_flow/ui/screens/TabbarScreens/taskScreen/taskScreen.dart';
import 'package:flutter/cupertino.dart';

class horizontalList extends StatelessWidget {


  void main(){
    runApp(
      new MaterialApp(
        initialRoute: '/',
        routes: <String, WidgetBuilder>{
          '/': (BuildContext context) => homeScreenClass(),
          'Orders': (BuildContext context) => new orderScreenClass(true),
        },
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 100.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          SizedBox(
            width: 10.0,
          ),
          InkWell(
            child: Container(
              width: 90.0,
              child: Text("Analytics"),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0)
                )
              ),
            ),
            onTap: (){
              
              Navigator.of(context).push(CupertinoPageRoute<void>(builder: (BuildContext context) => analyticsScreenClass()));
              print("tapped on ANALYTICS");
            },
          ),
          SizedBox(
            width: 10.0,
          ),
          InkWell(
            child: Container(
              
              width: 90.0,
              child: (Text("Customers")),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0)
                )
              ),
            ),
            onTap: (){
              Navigator.of(context).push(CupertinoPageRoute<void>(builder: (BuildContext context) => customerScreenClass()));
              print("tapped on CUSTOMER");
            },
          ),
          SizedBox(
            width: 10.0,
          ),
          InkWell(
            child: Container(
              
              width: 90.0,
              child: Text("Orders"),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0)
                )
              ),
            ),
            onTap: (){
              Navigator.of(context).push(CupertinoPageRoute<void>(builder: (BuildContext context) => orderScreenClass(true)));
              print("tapped on ORDERS");
            },
          ),
          SizedBox(
            width: 10.0,
          ),
          InkWell(
            child: Container(
              
              width: 90.0,
              child: Text("Tasks"),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0)
                )
              ),
            ),
            onTap: (){
              Navigator.of(context).push(CupertinoPageRoute<void>(builder: (BuildContext context)=>taskScreenClass()));
              print("tapped on TASKS");
            },
          ),
          SizedBox(
            width: 10.0,
          ),
          InkWell(
            child: Container(
              
              width: 90.0,
              child: Text("Sales"),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0)
                )
              ),
            ),
            onTap: (){
              print("tapped on SALES");
              Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>salesScreenClass()));
            },
          ),
          SizedBox(
            width: 10.0,
          ),
          InkWell(
            child: Container(
              
              width: 90.0,
              child: Text("Products"),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0)
                )
              ),
            ),
            onTap: (){
              Navigator.of(context).push(CupertinoPageRoute<void>(builder: (BuildContext context) => ProductScreen()));
              print("tapped on PRODUCTS");
            },
          ),
          SizedBox(
            width: 10.0,
          ),
        ],
      ),
    );
  }

  //was trying to optimise the horizontal list 
  //stucked on routing ontap to respective screens

  Widget horizontalListItem(String itemName, /*String imgPath */ ){
    return InkWell(
      onTap: (){
        print("TAPPED $itemName");
      },
      child: Container(
        width: 90.0,
        child: Text("Sales"),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
          borderRadius: BorderRadius.all(
            Radius.circular(5.0)
          )
        ),
      ),
    );
  }
  
}