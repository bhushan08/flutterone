import 'package:flutter/material.dart';
import 'package:onboarding_flow/ui/widgets/total_summary.dart';

class totalSummaryHomeScreen extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 140.0,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: listData.length,
        itemBuilder: (BuildContext context, int index) => Padding(
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0, bottom: 20.0),
          child: summaryItem(context, listData[index].name, listData[index].classData),
        ),
        /*children: <Widget>[
          GestureDetector(
            child: Container(
              width: 150.0,
              child: (Text("Total Revenue")),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0)
                )
              ),
            ),
            onTap: (){
              print("tapped on REVENU");
            },
          ),
          SizedBox(width: 20.0,),
          GestureDetector(
            child: Container(
              width: 150.0,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0)
                )
              ),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0, left: 5.0, right: 5.0, bottom: 15.0),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 25.0,
                        child: Text(
                          "Total Profit",
                          style: TextStyle(
                            fontSize: 20.0
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 40.0,
                        child: Text(
                          "34,000",
                          style: TextStyle(
                            fontSize: 35.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.blue
                          ),
                          ),
                      )
                    ],
                  )
               ],
              ),
            ),
            onTap: (){
              print("tapped on PROFIT");
            },
          ),
          SizedBox(width: 20.0,),
          summaryItem("Profit", 34000),
          SizedBox(width: 20.0,),
          GestureDetector(
            child: Container(
              
              width: 150.0,
              child: (Text("Total Views")),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0)
                )
              ),
            ),
            onTap: (){
              print("tapped on VIEWS");
            },
          ),
        ],*/
      ),
    );
  }

  Widget summaryItem(BuildContext context, String variableName, int variableData){
    return GestureDetector(
            child: Container(
              width: 150.0,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                border: Border.all(color: const Color(0xeeeeeeee), width: 2.5),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0)
                )
              ),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0, left: 5.0, right: 5.0, bottom: 15.0),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 25.0,
                        child: Text(
                          "Total $variableName",
                          style: TextStyle(
                            fontSize: 20.0
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 40.0,
                        child: Text(
                          "$variableData",
                          style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.blue
                          ),
                          ),
                      )
                    ],
                  )
               ],
              ),
            ),
            onTap: (){
              print("tapped on PROFIT");
            },
          );

  }

  var listData = [

    ListDataClass("Revenu", 34575),
    ListDataClass("Profit", 20000),
    ListDataClass("Views", 12312),

  ];
}


  class ListDataClass {
    final String name;
    final int classData;

    ListDataClass(this.name, this.classData);
  }