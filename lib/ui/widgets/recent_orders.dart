import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class recentOrders extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 300.0,
      //color: Colors.blue,
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          Container(
            height: 50.0,
            padding: const EdgeInsets.all(10.0),
            child: Text("Recent Orders", style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),),
          ),
          Container(
            color: Colors.white,
            height: 250.0,
            child: ListView(
              children: <Widget>[
                recentItemListItems("Daniel Wellington Classic", "John DOe", "Paypal", "#51238249", "Aug 11",123),
                recentItemListItems("Skater Dress", "Adele Camp", "Square", "#51238249", "Aug 11",260),
                recentItemListItems("The Mask", "Harvey Dent", "Paypal", "#51238249", "Aug 11",500),
              ],
            ),
          )
        ],
      ),
    );
  }
  

  Widget recentItemListItems(/*String imgPath,*/ String productName , String customerName, String paymentMode, String orderID, String dateOfPurchase, double costOfProduct){
    return Padding(
      padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
      child: GestureDetector( 
        onTap: (){
          print("tapped on one container of recent orders");
        },
        child: Container(
          height: 100.0,
          color: Colors.white,
          child: Row(
            children: <Widget>[
              Container(
                width: 70,
                height: 70,
                child: Icon(CupertinoIcons.profile_circled, size: 75.0,),
                /*decoration: BoxDecoration(
                  image: DecorationImage(
                    //image: AssetImage(imgPath),
                    fit: BoxFit.contain
                  )
                ),*/
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        height: 20.0,
                        child: Text(
                          productName,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          )
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        height: 20,
                        child: Text(
                          customerName,
                          style: TextStyle(
                          fontSize: 12.0 
                          )
                        ),
                      ),
                      SizedBox(
                        width: 20.0,
                      ),
                      Container(
                        height: 20,
                        child: Text(
                          paymentMode,
                          style: TextStyle(
                          fontSize: 12.0 
                          )
                        ),
                      ),
                      SizedBox(
                        width: 20.0,
                      ),
                      Container(
                        height: 20,
                        child: Text(
                          orderID,
                          style: TextStyle(
                          fontSize: 12.0 
                          )
                        ),
                      ),
                      SizedBox(
                        width: 20.0,
                      ),
                      Container(
                        height: 20,
                        child: Text(
                          dateOfPurchase,
                          style: TextStyle(
                          fontSize: 12.0 
                          )
                        ),
                      ),
                    ],
                  )
                ],
              ),
              Container(
                child: Text(
                  "\$$costOfProduct",
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.lightBlue
                  ),
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
          ),
        ),
      ),
    );
  }
}