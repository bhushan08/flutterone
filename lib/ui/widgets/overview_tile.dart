import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class overViewTile extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    var lineData = [
      LinearSales(0, 43),
      LinearSales(1, 20),
      LinearSales(2, 30),
      LinearSales(3, 15),
      LinearSales(4, 22),
      LinearSales(5, 52),
      LinearSales(6, 32),
      LinearSales(7, 62),
      LinearSales(8, 72),
      LinearSales(9, 92),
      LinearSales(10, 82),
    ];

    var lineSeries = [
      charts.Series(domainFn: (LinearSales sales,_) => sales.year,
      measureFn: (LinearSales sales,_) => sales.linear_sales,
      fillColorFn: (_,__) => charts.MaterialPalette.blue.shadeDefault,
      id:  "Sales",
      data: lineData
      )
    ];

    var lineChart = charts.LineChart(
      lineSeries,
      animate: true,
      defaultRenderer: new charts.LineRendererConfig(includePoints: true, includeArea: true),
      primaryMeasureAxis: new charts.NumericAxisSpec(
        renderSpec: new charts.SmallTickRendererSpec()
      ),
    );

    // TODO: implement build
    return Container(
      height: 275.0,
      //color: Colors.red,
      padding: const EdgeInsets.all(10.0),
      child: new Container(
        height: 50.0,
        //color: Colors.white,
        child: ListView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            Container(
              child: Text("Overview", style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),),
              height: 50.0,
              //color: Colors.green,
            ),
            Container(
              height: 200.0,
              child: lineChart,
            )
          ],
        ),
      ),
    );
  }
  
}

class LinearSales {
  final int year;
  final int linear_sales;

  LinearSales(this.year, this.linear_sales);
}
